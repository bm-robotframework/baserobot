*** Settings ***
Library                  String
Library                  DateTime
Library                  AppiumLibrary
Library                  Process
Library                  FakerLibrary                                                 locale=pt_BR
Library                  ${EXECDIR}/libs/4devs.py
# Library                ${EXECDIR}/libs/userInput.py
# Library                ${EXECDIR}/libs/randomPerson.robot

# Variaveis padrão
*** Variable ***
${app_package}           com.google.android.calculator
${app_activity}          com.android.calculator2.Calculator
${id_device}             0042973083
${platform}              Android
${appium_port}           49152
${appium_server}         http://127.0.0.1:${appium_port}/wd/hub
${bootstrap_port}        50000
${dafault_timeout}       20                                                           # Tempo maximo para encontrar o elemento.
${print_every_action}    False                                                        # Tirar print a cada Ação.

*** Keywords ***
# KeyWords padrão
Launch App
    [Arguments]                     ${device}                        ${package}                                      ${activity}
    Open Application                http://localhost:4723/wd/hub     platformName=Android                            deviceName=${device}             appPackage=${package}                appActivity=${activity}

Get Working Path
    Run Process                     pwd                              shell=True                                      alias=proc1
    ${WORKING_PATH}                 Get Process Result               proc1                                           stdout=true
    Set Suite Variable              ${WORKING_PATH}

Spawn Appium Server
    Get Working Path
    Start Process                   appium                           -p                                              ${appium_port}                   -bp                                  ${bootstrap_port}              stdout=${EXECDIR}/results/appium-log-${platform}.txt    shell=true
    Sleep                           5

Close Appium Server
    Run Keyword And Ignore Error    Close All Applications
    Terminate All Processes         kill=True
    Sleep                           5

Open App
    Spawn Appium Server
    Open Application                ${appium_server}                 platformName=${platform}                        deviceName=${id_device}          appPackage=${app_package}            appActivity=${app_activity}

Wait Element
    [Arguments]         ${element}                       ${timeout}=${dafault_timeout}                   ${fail_not_found}=True           ${fail_message}=Element not found
    ${present}          Run Keyword And Return Status    Wait Until Page Contains Element                ${element}                       ${timeout}                           False
    Run Keyword If      ${fail_not_found}                Run Keyword If                                  ${present}==False                Fail                                 msg=${fail_message}

Wait Element Appear and Disappear
    [Arguments]         ${element}                       ${max_timeout_appear}=2                         ${max_timeout_disappear}=120
    ${present}          Run Keyword And Return Status    Wait Until Element Is Visible                   ${element}                       ${max_timeout_appear}                False
    Run Keyword If      ${present}                       Wait Until Element Is Not Visible               ${element}                       ${max_timeout_disappear}

Exist Element
    [Arguments]         ${element}                       ${timeout}=${default_timeout}
    ${present}          Run Keyword And Return Status    Wait Until Element Is Visible                   ${element}                       ${timeout}                           False
    [Return]            ${present}

Click
    [Arguments]         ${element}                       ${timeout}=${dafault_timeout}
    Wait Element        ${element}                       ${timeout}
    Click Element       ${element}
    Run Keyword If      ${print_every_action}            Capture Page Screenshot

SendKeys
    [Arguments]         ${element}                       ${message}                                      ${timeout}=${dafault_timeout}    ${clear_text}=True                   ${press_home_key}=False
    Wait Element        ${element}                       ${timeout}
    Run Keyword If      ${clear_text}                    Run Keyword And Ignore Error                    Clear Element Text               ${element}
    Run Keyword If      ${press_home_key}                Press Key                                       ${element}                       \uE011${message}
    Run Keyword If      ${press_home_key}==False         Input Text                                      ${element}                       ${message}
    Run Keyword If      ${print_every_action}            Capture Page Screenshot

Swipe Up
    [Arguments]         ${element}
    ${size}             get Element Size                 ${element}
    ${location}         get element location             ${element}
    ${start_x}          Evaluate                         ${location['x']} + (${size['width']} * 0.5)
    ${start_y}          Evaluate                         ${location['y']} + (${size['height']} * 0.7)
    ${end_x}            Evaluate                         ${location['x']} + (${size['width']} * 0.5)
    ${end_y}            Evaluate                         ${location['y']} + (${size['height']} * 0.3)
    Swipe               ${start_x}                       ${start_y}                                      ${end_x}                         ${end_y}                             500
    Sleep               1
    Run Keyword If      ${print_every_action}            Capture Page Screenshot

Swipe Down
    [Arguments]         ${element}
    ${size}             get Element Size                 ${element}
    ${location}         get element location             ${element}
    ${start_x}          Evaluate                         ${location['x']} + (${size['width']} * 0.5)
    ${start_y}          Evaluate                         ${location['y']} + (${size['height']} * 0.3)
    ${end_x}            Evaluate                         ${location['x']} + (${size['width']} * 0.5)
    ${end_y}            Evaluate                         ${location['y']} + (${size['height']} * 0.7)
    Swipe               ${start_x}                       ${start_y}                                      ${end_x}                         ${end_y}                             500
    Sleep               1
    Run Keyword If      ${print_every_action}            Capture Page Screenshot

#Keywords Customizadas.
