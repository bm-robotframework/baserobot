*** Keywords ***
# KeyWords padrão
Open Navigator
    [Arguments]  ${url}  ${browser}=chrome  ${hide_browser}=False  ${recaptcha_solver}=False  ${adblock}=False
    ${ChromeOptions}  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver
    Run Keyword If  ${recaptcha_solver}==True  Call Method  ${ChromeOptions}  add_extension  ./utils/extensions/recaptcha_solver.crx
    Run Keyword If  ${adblock}==True  Call Method  ${ChromeOptions}  add_extension  ./utils//extensions/adblock.crx
    ${Options}  Call Method  ${ChromeOptions}  to_capabilities
    Run Keyword If  ${hide_browser}  Open Browser  ${url}  headless${browser}  desired_capabilities=${Options}
    Run Keyword If  ${hide_browser}==False  Open Browser  ${url}  ${browser}  desired_capabilities=${Options}
    Maximize Browser Window

Wait Element
    [Arguments]  ${element}  ${timeout}=${timeout}  ${fail_not_found}=True  ${fail_message}=Element not found
    ${present}  Run Keyword And Return Status  Wait Until Page Contains Element  ${element}  ${timeout}  False
    ${present}  Run Keyword And Return Status  Wait Until Element Is Visible  ${element}  ${timeout}  False
    Run Keyword If  ${fail_not_found} and ${present}==False  Fail  msg=${fail_message}

Wait Element Appear and Disappear
    [Arguments]  ${element}  ${max_timeout_appear}=2  ${max_timeout_disappear}=120
    ${present}  Run Keyword And Return Status  Wait Until Element Is Visible  ${element}  ${max_timeout_appear}  False
    Run Keyword If  ${present}  Wait Until Element Is Not Visible  ${element}  ${max_timeout_disappear}

Exist Element
    [Arguments]  ${element}  ${timeout}=${timeout}
    ${present}  Run Keyword And Return Status  Wait Until Element Is Visible  ${element}  ${timeout}  False
    [Return]  ${present}

Press Tab
    [Arguments]  ${element}
    Press Key  ${element}  \\009
    Run Keyword If  ${print_every_action}  Capture Page Screenshot

Click
    [Arguments]  ${element}  ${timeout}=${timeout}
    Wait Element  ${element}  ${timeout}
    Click Element  ${element}
    Run Keyword If  ${print_every_action}  Capture Page Screenshot

SendKeys
    [Arguments]  ${element}  ${message}  ${timeout}=${timeout}  ${clear_text}=True  ${press_home_key}=False
    Wait Element  ${element}  ${timeout}
    Run Keyword If  ${clear_text}  Run Keyword And Ignore Error  Clear Element Text  ${element}
    Run Keyword If  ${press_home_key}  Press Key  ${element}  \uE011${message}
    Run Keyword If  ${press_home_key}==False  Input Text  ${element}  ${message}
    Run Keyword If  ${print_every_action}  Capture Page Screenshot

#Keywords Customizadas.
