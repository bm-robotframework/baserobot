*** Keywords ***
Dado ${keyword}
    Run Keyword    ${keyword}

Quando ${keyword}
    Run Keyword    ${keyword}

Estão ${keyword}
    Run Keyword    ${keyword}

E ${keyword}
    Run Keyword    ${keyword}

Mas ${keyword}
    Run Keyword    ${keyword}

Given ${keyword}
    Run Keyword    ${keyword}

When ${keyword}
    Run Keyword    ${keyword}

Then ${keyword}
    Run Keyword    ${keyword}

And ${keyword}
    Run Keyword    ${keyword}

But ${keyword}
    Run Keyword    ${keyword}
