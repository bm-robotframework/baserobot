*** Settings ***
Resource  ${execdir}/main.resource
Resource  ${execdir}/pages/google.robot

*** Test Case ***
# robot -d results tests
Test 01: Search on Google.com
    Open Navigator    https://google.com
    Insert the time "amazon" in the search field
    Perform the search
    Check if the term "amazon" was found

*** Keywords ***
Insert the time "${term}" in the search field
    SendKeys  ${search.input}  ${term}

Perform the search
    Click  ${search.search_button}

Check if the term "${term}" was found
    Page Should Contain  ${term}
