import os
import pathlib

class Executor:

    def __init__(self):
        self.tests = []
        self.baseDir = ""
        self.resultFolder = "results"

    def addTest(self, test):
        self.tests.append(test)

    def setResultFolder(self, folder):
        self.resultFolder = folder

    def setBaseDir(self, basedir):
        self.baseDir = basedir

    def executeAll(self):
        os.system('cmd /c "cls"')
        resultPath = path.joinpath(f'{path}/{self.resultFolder}')
        os.system(f'cmd /c "robot -d {resultPath}"')

    def execute(self):
        os.system('cmd /c "cls"')
        for cmd in self.tests:
            name = str(cmd)
            name = name.split('.')[0] if '.' in name else name
            path = pathlib.Path().absolute()
            resultPath = path.joinpath(f'{path}/{self.resultFolder}/{name}')
            execPath = path.joinpath(f'{path}/{(self.baseDir if (len(self.baseDir) > 0) else cmd)}{"/" + cmd if (len(self.baseDir) > 0) else ""}')
            # print(resultPath)
            # print(execPath)
            os.system(f'cmd /c "robot -d {resultPath} {execPath}"')
