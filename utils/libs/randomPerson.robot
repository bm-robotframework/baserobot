*** Keywords ***
Random Person
    ${nome}  FakerLibrary.name
    ${cpf}  FakerLibrary.cpf
    ${rg}  FakerLibrary.rg
    ${cidade}  FakerLibrary.city
    ${cep}  FakerLibrary.postcode
    ${mae}  FakerLibrary.name female
    ${pai}  FakerLibrary.name male
    ${nascimento}  FakerLibrary.date Of Birth  minimum_age=18  maximum_age=115
    ${rua}  FakerLibrary.street name
    ${numero}  FakerLibrary.random int  min=100  max=9999

    ${cep}  Replace String  ${cep}  -  ${empty}
    ${cpf}  Replace String  ${cpf}  .  ${empty}
    ${cpf}  Replace String  ${cpf}  -  ${empty}
    ${nascimento}  Replace String  '${nascimento}'  date(  ${empty}
    ${nascimento}  Replace String  ${nascimento}  )  ${empty}
    ${nascimento}  Replace String  ${nascimento}  '  ${empty}
    @{w}  Split String  ${nascimento}  -
    ${nascimento}  Set Variable  ${w}[2]/${w}[1]/${w}[0]

    &{pessoa}  Create Dictionary  nome=${nome}  cpf=${cpf}  rg=${rg}  cidade=${cidade}
    ...        cep=${cep}  mae=${mae}  pai=${pai}  nascimento=${nascimento}  rua=Rua ${rua}  numero=${numero}
    [Return]  ${pessoa}
