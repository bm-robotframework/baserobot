import requests
import json
from robot.api.deco import keyword

url = "https://www.4devs.com.br/ferramentas_online.php"
headers = {'content-type': 'application/x-www-form-urlencoded;charset=utf-8'}

@keyword('Is Empty')
def Is_Empty(message):
    return len(str(message)) == 0

@keyword('Is Not Empty')
def Is_Not_Empty(message):
    return len(str(message)) > 0

@keyword('Is Equals Size')
def Is_Equals_Size(message, message_2):
    try:
        return len(message) == len(message_2)
    except:
        return False

@keyword('Is Equals')
def Is_Equals(message, message_2):
    try:
        return True if message == message_2 else False
    except:
        return False

@keyword('Is Not Equals')
def Is_Not_Equals(message, message_2):
    try:
        return True if message != message_2 else False
    except:
        return False

@keyword('Get String Size')
def Get_String_Size(message):
    return len(message)

@keyword('Is Equals Ignore Case')
def Is_Equals_Ignore_Case(message, message_2):
    try:
        return message.lower() == message_2.lower()
    except:
        return False

@keyword('Contains String')
def Contains_String(full_message, sub):
    var = full_message.find(sub)
    print("[" + full_message + "][" + sub + "]")
    print(var)
    return var >= 0

@keyword('Contains String Ignore Case')
def Contains_String_Ignore_Case(full_message, sub):
    var = full_message.lower().find(sub.lower())
    print("[" + full_message + "][" + sub + "]")
    print(var)
    return var >= 0

@keyword('Get Random Person')
def Get_Random_Person(sexo="I", pontuacao="S", idade=0, cep_estado="", txt_qtde=1, cep_cidade=""):
    body = {'acao': 'gerar_pessoa', 'sexo': sexo.upper(), 'pontuacao': pontuacao.upper(), 'idade': idade, 'cep_estado': cep_estado.upper(), 'txt_qtde': txt_qtde, 'cep_cidade': cep_cidade}
    r = requests.post(url, body, headers)
    r = Map(json.loads(r.text))
    print(r)
    return r

@keyword('Get Random CNPJ')
def Get_Random_CNPJ(pontuacao="S"):
    body = {'acao': 'gerar_cnpj', 'pontuacao': pontuacao.upper()}
    r = requests.post(url, body, headers)
    print(r.text)
    return r.text

@keyword('Get Random RG')
def Get_Random_RG(pontuacao="S"):
    body = {'acao': 'gerar_rg', 'pontuacao': pontuacao.upper()}
    r = requests.post(url, body, headers)
    print(r.text)
    return r.text

@keyword('Get Random CPF')
def Get_Random_CPF(estado="", pontuacao="S"):
    body = {'acao': 'gerar_cpf', 'cpf_estado': estado.upper(), 'pontuacao': pontuacao.upper()}
    r = requests.post(url, body, headers)
    print(r.text)
    return r.text

@keyword('Get Random Text')
def Get_Random_Text(tamanho=3, opcao="para", iniciar_loreipsum="s"):
    body = {'acao': 'gerar_textos', 'txt_quantidade': tamanho, 'opcoes': opcao.lower(), 'tipo_texto': 'texto', 'iniciar': iniciar_loreipsum.upper()}
    r = requests.post(url, body, headers)
    print(r.text)
    return r.text

class Map(dict):
    def __init__(self, *args, **kwargs):
        super(Map, self).__init__(*args, **kwargs)
        for arg in args:
            if isinstance(arg, dict):
                for k, v in arg.items():
                    self[k] = v

        if kwargs:
            for k, v in kwargs.items():
                self[k] = v

    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        super(Map, self).__setitem__(key, value)
        self.__dict__.update({key: value})

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        super(Map, self).__delitem__(key)
        del self.__dict__[key]
