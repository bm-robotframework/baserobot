from libs.Engine import Executor

r = Executor()
# Define pasta raiz dos tests
r.setBaseDir('tests')
# Define a pasta do resultado dos tests
r.setResultFolder('results')

# Lista os teste que devem ser executados
r.addTest('googleSearch.robot')
# r.addTest('mobile.robot')

# Executa todos os testes.
r.execute()
