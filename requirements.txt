robotframework-seleniumlibrary==3.2.0
robotframework-faker==5.0.0
robotframework-appiumlibrary==1.5.0.4
robotframework-requests==0.7.0
robotframework-sikulilibrary==2.0.0
robotframework==3.1.2
selenium==3.141.0
virtualenv==20.0.10
requests==2.23.0
urllib3==1.25.8
ibm-watson==4.3.0
ibm-cloud-sdk-core==1.5.1